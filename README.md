# Linux Tips

Dicas de coisas que coisas que vou usando no Linux.

## Utilizando arquivos de texto com parametros para download e etc.

`xargs arquivo.txt -L1 comando`

É interessante utilizar principalmente quando o comando não tem a opção de ler uma arquivo com os requerimentos.  
A opção `-L1` indica que cada linha do arquivo deverá ser executada separadamente,

* arquivo.txt: Arquivo de texto tem de ter as opções em cada linha separada.
* comando: é o comando onde quer utilizar cada linha, exemplo: wget, code install

## Gerando pendrive de boot

[Retirei essa dica do site Linuxize](https://linuxize.com/post/create-bootable-debian-10-usb-stick-on-linux/)

Encontrar o pendrive:

 `lsblk`

O drive ira aparecer na lista como sdx1 ou sdb1...no caso, iremos utilizar sem o número no momento de criar o disco, ou o sistema não irá aceitar o boot pelo drive.

Fazer o desmonte do dispositivo:

 `sudo umount /dev/sdb`

Criar o disco

`sudo dd bs=4M if=caminho/para/iso/desejada.iso of=/dev/sdb status-progress oflag=sync`

Irá criar o disco bootável no drive indicado.
